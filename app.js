const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");
const misRutas = require("./router/index");
const path = require("path");
const conex = require("./models/conexion");

const app = express();
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.engine("html", require("ejs").renderFile);

app.use(bodyparser.urlencoded({ extended: false }));
app.use(express.json());
app.use(misRutas);

// escuchar el servidor por el puerto 3000
const puerto = 5000;
app.listen(puerto, () => {
	console.log("Iniciado puerto 5000");
});

app.use((req, res, next) => {
	res.status(404).sendFile(__dirname + "/public/error.html");
});
