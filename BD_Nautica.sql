drop database if exists nautica;
create database if not exists nautica;

use nautica;

create table if not exists Usuarios(
IDUsuario int auto_increment primary key,
nombre varchar(30),
apellido varchar(30),
correo varchar(50),
contraseña varchar(255),
telefono varchar(15),
usuario varchar(30),
domicilio varchar(60),
estado boolean 
);


create table if not exists Muelles(
IDMuelle int auto_increment primary key,
estado boolean,
fk_Muelle_Cliente INT, 
constraint Muelle_Cliente
foreign key(fk_Muelle_Cliente)
references Usuarios(IDUsuario)
);



create table if not exists DiquesSeco(
IDDiqueSeco int auto_increment primary key,
disponibilidad boolean,
tamaño int,
estado boolean,
fk_Dique_Cliente INT, 
constraint Dique_Cliente
foreign key(fk_Dique_Cliente)
references Usuarios(IDUsuario)
);

create table if not exists Casetas(
IDCaseta int auto_increment primary key,
disponibilidad boolean,
estado boolean,
fk_Cliente int,
constraint fk_Caseta_Cliente
foreign key(fk_Cliente)
references Usuarios(IDUsuario)
);

create table if not exists Reservaciones(
IdReservacion int primary key,
fechaInicio date,
fechaFinal date,
rentaMensual int,
fk_Muelle INT,
foreign key(fk_Muelle) references Muelles(IDMuelle),
fk_DiqueSeco INT,
foreign key(fk_DiqueSeco) references DiquesSeco(IDDiqueSeco),
fk_Caseta INT,
foreign key(fk_Caseta) references Casetas(IDCaseta)
);

create table if not exists Admins(
IdAdministrador int primary key auto_increment,
nombre varchar(50),
telefono varchar(15),
estado boolean
);

CREATE PROCEDURE muellesDisponibles()
    select * from muelles inner join reservaciones on muelles.IDMuelle = reservaciones.fk_Muelle 
    where reservaciones.fechaInicio < current_date and reservaciones.fechaFinal < current_date;
;

CREATE TABLE espacios(
IDEspacio INT PRIMARY KEY AUTO_INCREMENT,
Nombre VARCHAR(25),
Descripcion VARCHAR(60),
CostoDiario INT,
urlImagen VARCHAR(255),
Estado varchar(100)
);

create table Peticiones(
IdPeticion int primary key auto_increment,
correo varchar(30),
nombre varchar(20),
apellido varchar(20),
descripcion varchar(30),
diasReservados int,
#opcionSel varchar(8),
tipoPago varchar(15),
fk_peticion_usuario INT,
constraint Peticion_Cliente
foreign key(fk_peticion_usuario)
references Usuarios(IDUsuario),
fk_peticion_espacio INT,
constraint Peticion_Espacio
foreign key(fk_peticion_espacio)
references espacios(IDEspacio)
);

select * from peticiones;




