const mysql = require('mysql2');
const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "nautica"

});

connection.connect((error) => {
    if (error) {
        console.log("El error de la conexion es : " +error);
    } else {
        console.log('Database is connected');
    }
});

module.exports = connection;