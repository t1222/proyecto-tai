const mysql = require('mysql2');

const conexion = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'nautica'
});

conexion.connect((error)=>{
	if (error) {
		console.log(error.message);
	} else {
		console.log('Se conectó con éxito');
	}
})

module.exports = conexion;