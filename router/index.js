const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const conexion = require('../models/conexion.js');
const bcrypt = require('bcrypt');
//const fileUpload = require('express-fileupload');
const multer = require('multer');
const path = require('path')
var login = false;
var email2 = '';

const storageEngine = multer.diskStorage({
		destination: (req, file, cb) => {
			cb(null, './public/assets/img/uploads');
		},
		filename: (req, file, cb) => {
			console.log(file)
			cb(null, Date.now() + path.extname(file.originalname))
		}
	});

const upload = multer({storage: storageEngine});

router.get('/pruebas', (req, res)=>{
	conexion.query('SELECT * FROM persona', (error, results)=>{
		if(error){
			throw error;
		}else{
			res.render('pruebas.html', {resultado: results});
		}
	});
});

router.get('/', (req, res)=>{
	res.render('index.html',{login:login});
})

router.get('/muelle', (req, res)=>{
	conexion.query('SELECT * FROM Espacios', (error, results)=>{
		if(error){
			throw error;
		}else{
			correos = email2;
			res.render('muelle.html', {results:results, correos});
		}
	});
})

router.get('/login',(req,res)=>{
    res.render('login.html')
})

router.get('/register',(req,res)=>{
    res.render('register.html')
})

router.get('/forgot-password',(req,res)=>{
    res.render('forgot-password.html')
})

router.post('/register',async (req,res)=>{
    let passwordHaash = await bcrypt.hash(req.body.password, 8)
    let usuarioObjeto = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
		usuario : req.body.usuario,
		domicilio : req.body.domicilio,
        telefono: req.body.telefono,
        correo: req.body.email,
        contraseña: passwordHaash,
        estado: 1
    }
    conexion.query('INSERT INTO usuarios SET ?', usuarioObjeto, async (error, results) => {
        if (error) {
            console.log(error);
        } else {
            res.render('register.html', {
                alert: true,
                alertTitle: "Registro",
                alertMessage: "Registro exitoso",
                alertIcon: "success",
                showConfirmButton:false,
                time: 1500,
                ruta: ''
            })
        }
    
    })
})

router.post('/auth', async (req,res)=>{
    const user = req.body.email
    const password = req.body.password
    if(user && password){
        conexion.query('SELECT * FROM usuarios WHERE correo = ?', [user], async (error, results) => {
            if(results.length == 0 || !(await bcrypt.compare(password, results[0].contraseña))){
                res.render('login.html', {
                    alert: true,
                    alertTitle: "Error",
                    alertMessage: "Usuario y/o contraseña incorrectos",
                    alertIcon: "error",
                    showConfirmButton:true,
                    time: 1500,
                    ruta: 'login'
                })
            }else{
                login = true
                res.render('login.html', {
                    alert: true,
                    alertTitle: "Bienvenido",
                    alertMessage: "Inicio de sesión exitoso",
                    alertIcon: "success",
                    showConfirmButton:false,
                    time: 1500,
                    ruta: 'perfil'
                })
				email2 = user;
            }
        })

    }else{
        res.render('login.html', {
            alert: true,
            alertTitle: "Error",
            alertMessage: "Por favor ingrese usuario y contraseña",
            alertIcon: "error",
            showConfirmButton:true,
            time: 1500,
            ruta: ''
        })
    }
})

// Paginas de administrador
router.get('/admin', (req, res)=>{
	res.render('admin_inicio.html');
})

router.get('/admin_usuarios', (req, res)=>{
    conexion.query('select * from Usuarios', (error, results)=>{
		if(error){
			throw error;
		}else{
			res.render('admin_usuarios.html', {results:results});
		}
	});

})

router.get('/admin_muelle', (req, res)=>{
	conexion.query('SELECT IDMuelle, Muelles.estado, nombre FROM Muelles inner join Usuarios ON Muelles.fk_Muelle_Cliente = Usuarios.IDUsuario', (error, results)=>{
		if(error){
			throw error;
		}else{
    res.render('admin_muelle.html', {results:results});
}
});

})

router.get('/admin_espacios', (req, res)=>{
	conexion.query('SELECT * FROM Espacios', (error, results)=>{
		if(error){
			throw error;
		}else{
			res.render('admin_espacios.html', {results:results});
		}
	});

})

router.get('/admin_estado', (req, res)=>{
	res.render('admin_estado.html');
})

router.get('/admin_peticiones', (req, res)=>{
	conexion.query('SELECT * FROM Peticiones inner join Espacios on peticiones.fk_peticion_espacio = espacios.IDEspacio;', (error, results)=>{
		if(error){
			throw error;
		}else{
			res.render('admin_peticiones.html', {results:results});
		}
	});

})


// Métodos funcionales
const crud = require('../controllers/crud.js');
router.post('/save', crud.save);
//router.post('/saveEspacios', crud.saveEspacios);
router.post('/saveMuelle', crud.saveMuelle);
router.post('/update', crud.update);
router.post('/saveU', crud.saveU);
//router.post('/updateEspacios', crud.updateEspacios);

router.post('/updateU', crud.updateU);
router.post('/saveMuelleCliente', crud.saveMuelleCliente);
router.post('/updateMuelle',crud.updateMuelle)

router.post('/updateEspacios', upload.single("imagen"),(req, res)=>{
	const id = req.body.id;
	const nombre = req.body.nombre;
    const descripcion = req.body.descripcion;
    const costo = req.body.costo;
	var url = req.file.destination + "/" + req.file.filename;
	url = url.slice(8);
    
	if(req.file) {
			console.log(req.file.path)
			console.log("Imagen subida exitosamente")
		} else {
			console.log("La imagen no fue subida exitosamente.")
		}

	
	conexion.query('UPDATE Espacios Set ? WHERE IDEspacio = ?', [{nombre:nombre, descripcion:descripcion, CostoDiario:costo, urlImagen:url}, id], (error, results)=>{
		if(error){
			console.log(error)
		}else{
			res.redirect('/admin_espacios');
		}
	})
})

router.post('/saveEspacios', upload.single("imagen"),(req, res)=>{
	const nombre = req.body.nombre;
    const descripcion = req.body.descripcion;
    const costo = req.body.costo;
	const dis = req.body.estado;
	var url = req.file.destination + "/" + req.file.filename;
	url = url.slice(8);
    console.log(dis)
	if(req.file) {
			console.log(req.file.path)
			console.log("Imagen subida exitosamente")
		} else {
			console.log("La imagen no fue subida exitosamente.")
		}

	

    conexion.query('INSERT INTO Espacios SET ?', {Nombre: nombre, Descripcion:descripcion, CostoDiario:costo, urlImagen:url,Estado:dis}, (error, results)=>{
        if(error){
            console.log(error)
        }else{
            res.redirect('/admin_espacios');
        }
    })
})

router.get('/edit/:IdPeticion', (req, res)=>{
	const id = req.params.IdPeticion;
	conexion.query('SELECT * FROM Peticiones inner join Espacios on peticiones.fk_peticion_espacio = espacios.IDEspacio; ', id,(error, results)=>{
		if(error){
			throw error;
		}else{
			res.render('admin_edit.html', {results:results});
		}
	});
})

router.get('/editU/:IDUsuario', (req, res)=>{
	const id = req.params.IDUsuario;
	conexion.query('SELECT * FROM Usuarios WHERE IDUsuario= ?', [id], (error, results)=>{
		if(error){
			throw error;
		}else{
			res.render('admin_editUser.html', {resultado: results[0]})
		}
	})
})

router.get('/editMuelle/:IDMuelle', (req, res)=>{
	const id = req.params.IDMuelle;
	conexion.query('SELECT * FROM Muelles WHERE IDMuelle= ?', [id], (error, results)=>{
		if(error){
			throw error;
		}else{
			res.render('admin_editMuelle.html', {results:results});
			//res.render('admin_editMuelle.html', {resultado: results[0]})
		}
	})
})


router.get('/deleteU/:IDUsuario', (req, res)=>{
	const id = req.params.IDUsuario;
	conexion.query('DELETE FROM Usuarios WHERE IDUsuario=?', [id], (error, results)=>{
		if(error){
			throw error;
		}else{
			res.redirect('/admin_usuarios');
		}
	})
})

router.get('/delete/:IdPeticion', (req, res)=>{
	const id = req.params.IdPeticion;
	conexion.query('DELETE FROM Peticiones WHERE IdPeticion=?', [id], (error, results)=>{
		if(error){
			throw error;
		}else{
			res.redirect('/admin_peticiones');
		}
	})
})

router.get('/deleteP/:IdPeticion', (req, res)=>{
	const id = req.params.IdPeticion;
	conexion.query('DELETE FROM Peticiones WHERE IdPeticion=?', [id], (error, results)=>{
		if(error){
			throw error;
		}else{
			res.redirect('/perfil');
		}
	})
})

router.get('/deleteMuelle/:IDMuelle', (req, res)=>{
	const id = req.params.IDMuelle;
	conexion.query('DELETE FROM Muelles WHERE IDMuelle=?', [id], (error, results)=>{
		if(error){
			throw error;
		}else{
			res.redirect('/admin_muelle');
		}
	})
})

// Codigo de los espacios

router.get('/editEspacio/:IDEspacio', (req, res)=>{
	const id = req.params.IDEspacio;
	conexion.query('SELECT * FROM espacios WHERE IDEspacio=?', [id], (error, results)=>{
		if(error){
			throw error;
		}else{
			res.render('admin_editEspacios.html', {resultado: results[0]})
		}
	})
})

router.get('/deleteEspacio/:IDEspacio', (req, res)=>{
	const id = req.params.IDEspacio;
	conexion.query('DELETE FROM Espacios WHERE IDEspacio=?', [id], (error, results)=>{
		if(error){
			throw error;
		}else{
			res.redirect('/admin_espacios');
		}
	})
})

router.get('/perfil', (req, res)=>{
	conexion.query('SELECT * FROM Peticiones inner join Espacios on peticiones.fk_peticion_espacio = espacios.IDEspacio where correo=?;', [email2], (error, results)=>{
		if(error){
			throw error;
		}else{
			res.render('perfil.html', {results:results});
		}
	});
})


module.exports = router;
