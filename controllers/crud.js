const conexion = require('../models/conexion.js');

exports.save = (req, res)=>{
    const email = req.body.correoElec;
    const name = req.body.nombre;
    const lastname = req.body.apellido;
    const description = req.body.descripcion;
    const dias = req.body.dias;
    const option = req.body.opciones;

    conexion.query('INSERT INTO Peticiones SET ?', {correo: email, nombre: name, apellido:lastname, descripcion: description, diasReservados:dias, fk_peticion_espacio: option}, (error, results)=>{
        if(error){
            console.log(error)
        }else{
            res.redirect('/admin_peticiones');
        }
    })

}

// Espacios

exports.saveEspacios = (req, res)=>{
    const nombre = req.body.nombre;
    const descripcion = req.body.descripcion;
    const costo = req.body.costo;
    const url = req.body.imagen;

    conexion.query('INSERT INTO Espacios SET ?', {Nombre: nombre, Descripcion:descripcion, CostoDiario:costo, urlImagen:url}, (error, results)=>{
        if(error){
            console.log(error)
        }else{
            res.redirect('/admin_espacios');
        }
    })
}

exports.updateEspacios = (req, res)=>{
    const id = req.body.id
    const nombre = req.body.nombre;
    const descripcion = req.body.descripcion;
    const costo = req.body.costo;
    const url = req.body.imagen;
    const disp = req.body.estado

    conexion.query('UPDATE Espacios Set ? WHERE IDEspacio = ?', [{nombre:nombre, descripcion:descripcion, CostoDiario:costo, urlImagen:url,Estado:disp}, id], (error, results)=>{
        if(error){
            console.log(error)
        }else{
            res.redirect('/admin_espacios');
        }
    })
}

exports.saveMuelle = (req, res)=>{
    const email = req.body.correoElec;
    const name = req.body.nombre;
    const lastname = req.body.apellido;
    const description = req.body.descripcion;
    const dias = req.body.dias;
    const option = req.body.opciones;
    const tipoPago = req.body.opcPago;

    conexion.query('INSERT INTO Peticiones SET ?', {correo: email, nombre: name, apellido:lastname, descripcion: description, diasReservados:dias, tipoPago:tipoPago, fk_peticion_espacio: option}, (error, results)=>{
        if(error){
            console.log(error)
        }else{
            res.redirect('/muelle');
        }
    })

}

exports.saveMuelleCliente = (req, res)=>{
    const id = req.body.IDMuelle;
    const state = req.body.estado;
    const cliente = req.body.IDUsuario;
    console.log(req.body.IDUsuario)
    console.log(cliente)

    conexion.query('INSERT INTO Muelles SET ?', {IDMuelle: id, estado: state, fk_Muelle_Cliente:cliente}, (error, results)=>{
        if(error){
            console.log(error)
        }else{
            res.redirect('/admin_muelle');
        }
    })

}

exports.saveU = (req, res)=>{
    const email = req.body.correoElec;
    const name = req.body.nombre;
    const appe = req.body.apellido;
    const password = req.body.contraseña;
    const cell = req.body.telefono;
    const option = req.body.estado;

    conexion.query('INSERT INTO Usuarios SET ?', {correo: email, nombre: name, apellido: appe,contraseña:password, telefono: cell, estado: option}, (error, results)=>{
        if(error){
            console.log(error)
        }else{
            res.redirect('/admin_usuarios');
        }
    })

}

exports.updateU= (req, res)=>{
    const id = req.body.id;
    const email = req.body.correoElec;
    const name = req.body.nombre;
    const appe = req.body.apellido;
    const password = req.body.contraseña;
    const cell = req.body.telefono;
    const option = req.body.estado;
    
    conexion.query('UPDATE Usuarios Set ? WHERE IDUsuario = ?', [{correo: email, nombre: name,apellido: appes, contraseña:password, telefono: cell, estado: option}, id], (error, results)=>{
        if(error){
            console.log(error)
        }else{
            res.redirect('/admin_usuarios');
        }
    })
}



exports.update= (req, res)=>{

    const id = req.body.id;
    const email = req.body.correoElec;
    const name = req.body.nombre;
    const lastname = req.body.apellido;
    const description = req.body.descripcion;
    const dias = req.body.dias;
    const tipoPago = req.body.opcPago;
    const option = req.body.opciones;

    conexion.query('UPDATE Peticiones Set ? WHERE IdPeticion = ?', [{correo: email, nombre: name, apellido:lastname, descripcion: description, diasReservados:dias, tipoPago:tipoPago, fk_peticion_espacio: option}, id], (error, results)=>{
        if(error){
            console.log(error)
        }else{
            res.redirect('/admin_peticiones');
        }
    })


}


exports.updateMuelle= (req, res)=>{
    const id = req.body.IDMuelle;
    const state = req.body.estado;
    const cliente = req.body.IDUsuario;
    console.log(req.body.IDMuelle)
    conexion.query('UPDATE Muelles Set ? WHERE IDMuelle = ?', [{ estado: state, fk_Muelle_Cliente:cliente}, id], (error, results)=>{
        if(error){
            console.log(error)
        }else{
            res.redirect('/admin_muelle');
        }
    })
}
